﻿[
  {
    "name": "Rich text editor",
    "alias": "rte",
    "view": "rte",
    "icon": "icon-article"
  },
  {
    "name": "Image",
    "alias": "media",
    "view": "media",
    "icon": "icon-picture",
    "config": {
        "style": "margin-bottom: 10px;"     
    }
  },
  {
    "name": "Macro",
    "alias": "macro",
    "view": "macro",
    "icon": "icon-settings-alt"
  },
  {
    "name": "Embed",
    "alias": "embed",
    "view": "embed",
    "icon": "icon-movie-alt"
  },
  {
    "name": "H1",
    "alias": "headline1",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "markup": "<h1 class='align-center'>#value#</h2>"
    }
  },
  {
    "name": "H2",
    "alias": "headline2",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
      "markup": "<h2 class='align-center'>#value#</h2>"
    }
  },
  {
    "name": "H3",
    "alias": "headline3",
    "view": "textstring",
    "icon": "icon-coin",
    "config": {
        "markup": "<h3 class='align-center'>#value#</h2>"
    }
  },
  {
    "name": "Quote",
    "alias": "quote",
    "view": "textstring",
    "icon": "icon-quote",
    "config": {
      "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-style: italic; font-size: 18px",
      "markup": "<blockquote>#value#</blockquote>"
    }
  },
   {
    "name": "Doc Type",
    "alias": "docType",
    "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
    "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
    "icon": "icon-item-arrangement",
    "config": {
      "allowedDocTypes": ["papiersoorten", "ophangsysteem", "schaduwvoeglijst"],
      "nameTemplate": "",
      "enablePreview": true,
      "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
      "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
      "previewCssFilePath": "",
      "previewJsFilePath": ""
    }
  }
]