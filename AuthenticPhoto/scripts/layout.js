﻿window.addEventListener('load', function () {
    let images = document.querySelectorAll(".lazyload");

    lazyload(images);
    $.fn.matchHeight._update()
    console.log("lazyload called");
}, false);

$(document).ready(function () {

    $("#loader").fadeOut(500);
    $(".text-slide-wrapper").hover(
        function () {
            $(this).find(".text-slide").animate({ bottom: "0px" }, 400, "easeOutQuart");
        },
        function () {
            $(this).find(".text-slide").animate({ bottom: "-87px" }, 400, "easeOutQuart");
        }
    );

    $('#languageSelector').on('change', function () {
        var homepage = $(this).find('option:selected', this).data('homepage');

        window.location.href = homepage;
    });

    if ($("#video").length) {
        var video = $("#video");
        var playButton = $("#play-pause");
        var muteButton = $("#mute");
        $("#play-pause").on("click", function (e) {
            if ($("#video").get(0).paused) {
                $("#play-pause").removeClass("fa-play-circle").addClass("fa-pause-circle");
                $("#video").get(0).play();
            } else {
                $("#play-pause").removeClass("fa-pause-circle").addClass("fa-play-circle");
                $("#video").get(0).pause();
            }
        });
        $("#video").on("click", function (e) {
            e.preventDefault();
        });
        $("#mute").on("click", function (e) {
            if ($(video).prop('muted')) {
                $(video).prop('muted', false);
            } else {
                $(video).prop('muted', true);
            }
        });
    }

    
    /* Set the matchMedia */
    if (window.matchMedia('(min-width: 1280px)').matches) {
        $(".navbar-grey-light").addClass("dropdown-onhover");
    } else {
        $(".navbar-grey-light").removeClass("dropdown-onhover");
    }
    

});

document.addEventListener("DOMContentLoaded", function (event) {
    window.onscroll = positionTopStickyNavigation;
    var sticky = document.getElementById("topstickynavigation");
    //var stickyOffset = sticky.offsetHeight + "px";
    var stickyOffset = "214px";
    // if (!stickyOffset) {
    //     stickyOffset = "214px";
    // }
    var main = document.getElementsByTagName("main")[0];
    var origMainOffset = main.style.marginTop;
    function positionTopStickyNavigation() {
        if (window.pageYOffset > 0) {
            sticky.classList.add("sticky");
            console.log(stickyOffset);
            main.style.marginTop = stickyOffset;
        } else {
            sticky.classList.remove("sticky");
            main.style.marginTop = origMainOffset;
        }
    }
});

$(function () {
    $(".griditem").matchHeight();
});
