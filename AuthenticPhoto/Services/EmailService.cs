﻿using AuthenticPhoto.Extensions;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;

namespace AuthenticPhoto.Services
{
    public static class EmailService
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Send the e-mail.
        /// </summary>
        /// <param name="umbraco"></param>
        /// <param name="emailFrom"></param>
        /// <param name="emailFromName"></param>
        /// <param name="emailTo"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="emailCc"></param>
        /// <param name="emailBcc"></param>
        /// <param name="addFiles"></param>
        /// <param name="attachments"></param>
        public static void SendEmail(
            string emailFrom,
            string emailFromName,
            string emailTo,
            string subject,
            string body,
            bool useCredentials = false,
            string emailCc = "",
            string emailBcc = "",
            //EmailType? emailType = null,
            bool addFiles = false,
            Dictionary<string, MemoryStream> attachments = null,
            string replyTo = ""
            
            )
        {
            //Make the MailMessage and set the e-mail address.
            MailMessage message = new MailMessage();
            message.From = new MailAddress(emailFrom, emailFromName);

            //Split all the e-mail addresses on , or ;.
            char[] splitChar = { ',', ';' };

            //Remove all spaces.
            emailTo = emailTo.Trim();
            emailCc = emailCc.Trim();
            emailBcc = emailBcc.Trim();
            replyTo = replyTo.Trim();

            //Split emailTo.
            string[] toArray = emailTo.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            foreach (string to in toArray)
            {
                //Check if the e-mail is valid.
                if (to.IsValidEmail())
                {
                    //Loop through all e-mail addressen in toArray and add them in the to.
                    message.To.Add(new MailAddress(to));
                }
            }

            //Split emailCc.
            string[] ccArray = emailCc.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cc in ccArray)
            {
                //Check if the e-mail is valid.
                if (cc.IsValidEmail())
                {
                    //Loop through all e-mail addressen in ccArray and add them in the cc.
                    message.CC.Add(new MailAddress(cc));
                }
            }

            //Split emailBcc.
            string[] bccArray = emailBcc.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            foreach (string bcc in bccArray)
            {
                //Check if the e-mail is valid.
                if (bcc.IsValidEmail())
                {
                    //Loop through all e-mail addressen in bccArray and add them in the bcc.
                    message.Bcc.Add(new MailAddress(bcc));
                }
            }

            //Split replyTo.
            string[] replayArray = replyTo.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            foreach (string to in replayArray)
            {
                //Check if the e-mail is valid.
                if (to.IsValidEmail())
                {
                    //Loop through all e-mail addressen in toArray and add them in the to.
                    message.ReplyToList.Add(new MailAddress(to));
                }
            }

            if (addFiles && attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    var att = new Attachment(attachment.Value, attachment.Key);
                    message.Attachments.Add(att);
                }
            }

            //Set the rest of the e-mail data.
            message.Subject = subject;
            message.SubjectEncoding = Encoding.UTF8;
            message.Body = body;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;

            //Only send the email if there is a to address.
            if (message.To.Any())
            {
                try
                {
                    if (useCredentials)
                    {
                        SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                        string smtpHost = section.Network.Host;
                        int port = section.Network.Port;
                        bool enableSsl = section.Network.EnableSsl;
                        string smtpUser = section.Network.UserName;
                        string smtpPass = section.Network.Password;

                        using (var smtpClient = new SmtpClient(smtpHost, port))
                        {
                            smtpClient.EnableSsl = enableSsl;

                            smtpClient.Credentials = new System.Net.NetworkCredential(smtpUser, smtpPass);

                            smtpClient.Send(message);
                        }

                    }
                    else
                    {
                        using (var smtpClient = new SmtpClient())
                        {
                            smtpClient.Send(message);
                        }       
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            //Clear the resources.
            message.Dispose();
        }
    }
}