﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Web.Editors;
using Umbraco.Web.Models.ContentEditing;

namespace AuthenticPhoto.Models
{
    public class ContentModelModifier : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            EditorModelEventManager.SendingContentModel += (sender, e) =>
            {
                IEnumerable<ContentPropertyDisplay> gridDisplayProperties = e.Model.Properties.Where(x => x.Editor == "Umbraco.Grid");
                foreach (ContentPropertyDisplay gridDisplayProperty in gridDisplayProperties)
                {
                    gridDisplayProperty.HideLabel = false;
                }
            };
        }
    }
}