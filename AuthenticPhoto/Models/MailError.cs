﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class MailError
    {
        public string RemoteAddress { get; set; }
        public string UserAgent { get; set; }
        public string Referrer { get; set; }
        public string PageUrl { get; set; }

        public string Property { get; set; }
        public string ErrorPage { get; set; }
    }
}