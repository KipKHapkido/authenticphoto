﻿using AuthenticPhoto.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Core.Logging;

namespace AuthenticPhoto.Models
{
    public class Mail
    {
        public void SendMail(MailError model)
        {
            var contactData = string.Format("Date: {6}<br>RemoteAddress: {0}<br>UserAgent: {1}<br>Referrer: {2}<br>Page url: {3}<br>Property?: {4}<br>Error Page: {5}", model.RemoteAddress, model.UserAgent, model.Referrer, model.PageUrl, model.Property, model.ErrorPage, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
            string toEmail = ConfigurationManager.AppSettings["SendToErrorMail"];
            string ccEmail = ConfigurationManager.AppSettings["SendCcErrorMail"];
            try
            {
                EmailService.SendEmail("authentic@authenticbrussels.onmicrosoft.com", "authentic@authenticbrussels.onmicrosoft.com", toEmail, "AuthenticPhoto - ontbrekende afbeelding", string.Format("<p>Opgevangen fout - Ontbrekende afbeelding:</p><br><br>{0}<br><br><br><small>This is an automatically sent message.</small>", contactData), useCredentials: true, emailCc: ccEmail);
            }
            catch (Exception ex)
            {
                LogHelper.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "msg", ex);
            }
        }
    }
}