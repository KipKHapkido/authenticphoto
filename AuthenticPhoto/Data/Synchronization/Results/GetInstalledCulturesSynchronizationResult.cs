﻿using System.Collections.Generic;

namespace AuthenticPhoto.Data.Synchronization.Results
{
    public class GetInstalledCulturesSynchronizationResult : SynchronizationResult
    {
        public List<InstalledCulture> CultureCodes { get; set; } 
    }
}
