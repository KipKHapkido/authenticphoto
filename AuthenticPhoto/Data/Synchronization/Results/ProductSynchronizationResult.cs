﻿namespace AuthenticPhoto.Data.Synchronization.Results
{
    public class ProductSynchronizationResult : SynchronizationResult
    {
        public Product Product { get; set; }
        public int DocumentId { get; set; }
    }
}
