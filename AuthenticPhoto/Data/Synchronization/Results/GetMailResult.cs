﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Data.Synchronization.Results
{
    public class GetMailResult : SynchronizationResult
    {
        public List<MailDefinition> MailDefinitions { get; set; }
    }
}