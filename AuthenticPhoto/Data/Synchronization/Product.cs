﻿namespace AuthenticPhoto.Data.Synchronization
{
    public sealed class Product : ProductDbEntity
    {
        public ProductFamily ProductFamily { get; set; }
    }
}