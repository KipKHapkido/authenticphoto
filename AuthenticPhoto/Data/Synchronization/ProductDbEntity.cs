﻿namespace AuthenticPhoto.Data.Synchronization
{
    public abstract class ProductDbEntity
    {
        public string ProductDbIdentifier { get; set; }
        public string CultureCode { get; set; }
        public string Name { get; set; }
    }
}