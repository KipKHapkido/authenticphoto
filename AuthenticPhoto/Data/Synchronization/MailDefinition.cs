﻿namespace AuthenticPhoto.Data.Synchronization
{
    public class MailDefinition
    {
        public string CultureCode { get; set; }
        public string View { get; set; }
        public string Html { get; set; }
    }
}