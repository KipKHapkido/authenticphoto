﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Dynamics;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace AuthenticPhoto.Controllers
{
    public class AuthenticApiController : UmbracoApiController
    {
        private string normalizePropValue(string propValue, UmbracoHelper umbracoHelper)
        {
            if (propValue.StartsWith("umb:")) {
                Udi mainImageUdi;
                if (Udi.TryParse(propValue, out mainImageUdi))
                {
                    var mediaItem = umbracoHelper.TypedMedia(mainImageUdi);
                    if (mediaItem != null)
                    {
                        return mediaItem.Url;
                    }
                }
            }
            return propValue;
        }

        private void AddExpandoProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        private Dictionary<string, string> getContentPropValues(UmbracoHelper umbracoHelper, int contentId, string[] propNames)
        {
            var content = umbracoHelper.Content(contentId);
            if (content == null || content.GetType() == typeof(DynamicNull))
            {
                throw new Exception("No content found for id " + contentId);
            }
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach(string propName in propNames)
            {
                var prop = ((IPublishedContent)content).GetProperty(propName);
                if (prop == null)
                {
                    throw new Exception("Content for " + contentId + " has no property " + propName);
                }
                dictionary.Add(propName, prop.DataValue.ToString());
            }
            return dictionary;
        }

        [HttpGet]
        public HttpResponseMessage ContentProp([FromUri]int contentId, [FromUri]string propName)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            Dictionary<string, string> propsDict = getContentPropValues(umbracoHelper, contentId, new string[] { propName });
            var result = propsDict[propName];
            var response = Request.CreateResponse(HttpStatusCode.OK, result);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
        [HttpGet]
        public HttpResponseMessage ContentProps([FromUri]string contentIdsCsv, [FromUri]string propNamesCsv)
        {
            var contentIds = contentIdsCsv.Split('-');
            var propNames = propNamesCsv.Split('-');

            dynamic obj = new ExpandoObject();

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            foreach(string contentId in contentIds)
            {
                Dictionary<string, string> propsDict = getContentPropValues(umbracoHelper, Int32.Parse(contentId), propNames);
                dynamic contentEntry = new ExpandoObject();
                foreach (string propKey in propsDict.Keys) {
                    string propValue = normalizePropValue(propsDict[propKey], umbracoHelper);
                    AddExpandoProperty(contentEntry, propKey, propValue);
                }
                AddExpandoProperty(obj, contentId, contentEntry);
            }

            string result = JsonConvert.SerializeObject(obj);
            var response = Request.CreateResponse(HttpStatusCode.OK, result);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}