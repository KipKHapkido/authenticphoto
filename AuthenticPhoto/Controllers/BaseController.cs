﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace AuthenticPhoto.Controllers
{
    public class BaseController : SurfaceController
    {
        public string RemoteAddress { get; set; }
        public string UserAgent { get; set; }
        public string Referrer { get; set; }
        public string PageUrl { get; set; }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            HttpRequestBase PageReq = HttpContext.Request;

            RemoteAddress = PageReq.UserHostAddress;
            UserAgent = PageReq.UserAgent;
            Referrer = (PageReq.UrlReferrer != null) ? PageReq.UrlReferrer.OriginalString : "";
            PageUrl = PageReq.RawUrl;

            return base.BeginExecuteCore(callback, state);
        }
    }
}