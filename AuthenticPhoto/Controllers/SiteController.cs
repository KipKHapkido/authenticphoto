﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace AuthenticPhoto.Controllers
{
    public class SiteController : RenderMvcController
    {
        // GET: Site
        public override ActionResult Index(RenderModel model)
        {
            // don't do a redirect for sitemap.xml
            if(Request != null && Request.Url != null && Request.Url.AbsolutePath != null && !Request.Url.AbsolutePath.ToLower().Contains("sitemapxml"))
            {
                string language;
                if(Request.UserLanguages != null)
                {
                    var userLanguages = Request.UserLanguages[0];
                    language = userLanguages.Substring(0, 2);
                } else
                {
                    language = "nl";
                }

                Site website = (Site)Umbraco.TypedContent(CurrentPage.Id);
                IEnumerable<Homepage> homes = website.Children<Homepage>();
                Homepage home = homes.FirstOrDefault(x => x.GetCulture().ToString().Contains(language));
                if (home == null)
                {
                    home = homes.First();
                }

                return Redirect(home.Url);
            }

            return CurrentTemplate(UmbracoContext.PublishedContentRequest.PublishedContent);
        }
    }
}