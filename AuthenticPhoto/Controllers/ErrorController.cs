﻿using AuthenticPhoto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace AuthenticPhoto.Controllers
{
    public class ErrorController : BaseController
    {
        private readonly Mail _mail;

        public ErrorController()
        {
            _mail = new Mail();
        }

        // GET: Error
        public ActionResult Mail(string property, string errorPage)
        {
            MailError error = new MailError
            {
                RemoteAddress = this.RemoteAddress,
                UserAgent = this.UserAgent,
                Referrer = this.Referrer,
                PageUrl = this.PageUrl,
                Property = property,
                ErrorPage = errorPage
            };

            _mail.SendMail(error);

            return new EmptyResult();
        }
    }
}