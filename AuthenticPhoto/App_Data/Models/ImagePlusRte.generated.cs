//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>ImagePlusRTE</summary>
	[PublishedContentModel("imagePlusRTE")]
	public partial class ImagePlusRte : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "imagePlusRTE";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public ImagePlusRte(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<ImagePlusRte, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Afbeelding
		///</summary>
		[ImplementPropertyType("afbeelding")]
		public IPublishedContent Afbeelding
		{
			get { return this.GetPropertyValue<IPublishedContent>("afbeelding"); }
		}

		///<summary>
		/// Tekst
		///</summary>
		[ImplementPropertyType("tekst")]
		public Our.Umbraco.Vorto.Models.VortoValue Tekst
		{
			get { return this.GetPropertyValue<Our.Umbraco.Vorto.Models.VortoValue>("tekst"); }
		}
	}
}
